package ru.fbtw.strange_sort.core;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ru.fbtw.strange_sort.core.container.PairContainer;
import ru.fbtw.strange_sort.util.SortLib;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class SequenceProviderTest {

	public static final int ARR_LEN = 29;
	public static final int TEST_COUNT = 10;

	private static Stream<Arguments> getRandomArray() {
		Random random = new Random();
		List<Object> res = new ArrayList<>();

		for (int i = 0; i < TEST_COUNT; i++) {
			Integer[] array = IntStream.range(0, ARR_LEN)
					.map(v -> random.nextInt())
					.boxed()
					.toArray(Integer[]::new);
			res.add(array);
		}

		return res.stream()
				.map(Arguments::of);
	}

	@ParameterizedTest
	@MethodSource("getRandomArray")
	void getSortSwapSequence(Integer[] arr) {
		printArray(arr, "Исходный массив:");
		Integer[] expected = Arrays.copyOf(arr, arr.length);
		Arrays.sort(expected);
		printArray(expected, "Отсортированный массив:");

		List<PairContainer<Integer, Integer>> sortSwapSequence = SequenceProvider.getSortSwapSequence(arr);
		sortSwapSequence.forEach(System.out::println);
		sortSwapSequence.forEach(v -> SortLib.swap(arr, v.getFirst(), v.getSecond()));
		Assertions.assertArrayEquals(expected, arr);
		System.out.println("---------------------------------------");
	}


	<T> void printArray(T[] arr, String message) {
		System.out.println(message);
		System.out.println();
		System.out.println(Arrays.toString(arr));
		System.out.println();
	}
}