package ru.fbtw.strange_sort;


import ru.fbtw.strange_sort.core.SequenceProvider;
import ru.fbtw.strange_sort.core.container.PairContainer;

import java.util.List;

public class App {
	public static void main(String[] args) {
		/*Integer[] arr = new Integer[10];
		for(int i = 0; i < arr.length; i++){
			arr[i] = arr.length - i;
		}*/
		Integer[] arr = new Integer[]{0,10,5,6};
		List<PairContainer<Integer, Integer>> sortSwapSequence = SequenceProvider.getSortSwapSequence(arr);
		sortSwapSequence.forEach(System.out::println);
	}
}
