package ru.fbtw.strange_sort.core;

import ru.fbtw.strange_sort.core.container.IndexedContainer;
import ru.fbtw.strange_sort.core.container.IndexedContainerProvider;
import ru.fbtw.strange_sort.core.container.PairContainer;
import ru.fbtw.strange_sort.util.SortLib;

import java.util.*;

public class SequenceProvider {

	@SuppressWarnings("unchecked")
	public static <T extends Comparable<T>> List<PairContainer<Integer, Integer>> getSortSwapSequence(final T[] src) {
		IndexedContainerProvider<T> provider = new IndexedContainerProvider<>();

		IndexedContainer<T>[] indexedContainers = (IndexedContainer<T>[]) Arrays.stream(src)
				.map(provider)
				.toArray(IndexedContainer[]::new);

		int n = indexedContainers.length;
		SortLib.mergeSort(indexedContainers, 0, n);


		return generatePairs(indexedContainers);
	}

	private static <T extends Comparable<T>> List<PairContainer<Integer, Integer>> generatePairs(
			IndexedContainer<T>[] containers) {
		List<PairContainer<Integer, Integer>> res = new ArrayList<>();

		boolean[] checked = new boolean[containers.length];
		for (int i = 0; i < containers.length; i++) {
			generatePairs(containers, res, i, checked);
		}

		return res;
	}

	private static <T extends Comparable<T>> void generatePairs(
			IndexedContainer<T>[] containers,
			List<PairContainer<Integer, Integer>> res,
			int index, boolean[] checked
	) {
		// Если точка посещена пропускаем
		if (checked[index]) return;

		checked[index] = true;
		Integer next = containers[index].getSecond();

		// Если пришли из посещенной точки то пропускаем ее
		if (checked[next]) return;

		res.add(new PairContainer<>(next, index));
		generatePairs(containers, res, next, checked);

	}
}
