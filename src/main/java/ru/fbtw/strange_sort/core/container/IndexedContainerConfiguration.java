package ru.fbtw.strange_sort.core.container;

import java.util.function.Function;

public class IndexedContainerConfiguration {
	private final int beginIndex;
	private final Function<Integer,Integer> supplier;

	public IndexedContainerConfiguration(int beginIndex, Function<Integer, Integer> supplier) {
		this.beginIndex = beginIndex;
		this.supplier = supplier;
	}

	public int getBeginIndex() {
		return beginIndex;
	}

	public Function<Integer, Integer> getSupplier() {
		return supplier;
	}
}
