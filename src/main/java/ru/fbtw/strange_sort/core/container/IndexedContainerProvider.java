package ru.fbtw.strange_sort.core.container;

import java.util.function.Function;

public class IndexedContainerProvider<T extends Comparable<T>>
		implements Function<T, IndexedContainer<T>> {

	private static final IndexedContainerConfiguration DEFAULT_CONFIG =
			new IndexedContainerConfiguration(0, prev -> ++prev);

	private final IndexedContainerConfiguration config;
	private int index;


	public IndexedContainerProvider() {
		this(DEFAULT_CONFIG);
	}


	public IndexedContainerProvider(IndexedContainerConfiguration config) {
		this.config = config;
		this.index = this.config.getBeginIndex();
	}


	@Override
	public IndexedContainer<T> apply(T t) {
		IndexedContainer<T> container = new IndexedContainer<>(t, index);
		updateIndex();
		return container;
	}

	private void updateIndex() {
		index = config.getSupplier()
				.apply(index);
	}
}
