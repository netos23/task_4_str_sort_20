package ru.fbtw.strange_sort.core.container;

public class IndexedContainer<T extends Comparable<T>>
		implements Pair<T, Integer>, Comparable<IndexedContainer<T>> {
	private final T src;
	private final int index;

	IndexedContainer(T src, int index) {
		this.src = src;
		this.index = index;
	}

	public T getFirst() {
		return src;
	}

	public Integer getSecond() {
		return index;
	}

	@Override
	public int compareTo(IndexedContainer<T> o) {
		return this.getFirst().compareTo(o.getFirst());
	}

	@Override
	public int hashCode() {
		return src.hashCode();
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean equals(Object obj) {
		if (!(obj instanceof IndexedContainer)) {
			return false;
		} else {
			IndexedContainer<T> container = (IndexedContainer<T>) obj;
			return container.getFirst().equals(this.getFirst())
					&& container.getSecond().equals(this.getSecond());
		}
	}

	@Override
	public String toString() {
		return src.toString();
	}
}
