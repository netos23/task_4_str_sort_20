package ru.fbtw.strange_sort.core.container;

import java.util.Objects;

public class PairContainer<K, V> implements Pair<K, V> {
	private final K first;
	private final V second;

	public PairContainer(K first, V second) {
		this.first = first;
		this.second = second;
	}

	public K getFirst() {
		return first;
	}

	public V getSecond() {
		return second;
	}

	@Override
	public String toString() {
		return String.format("%s : %s", first, second);
	}

	@Override
	public int hashCode() {
		return Objects.hash(first, second);
	}
}
