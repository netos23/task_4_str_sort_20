package ru.fbtw.strange_sort.core.container;

public interface Pair<K, V> {
	K getFirst();

	V getSecond();
}
